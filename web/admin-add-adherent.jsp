<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <%@ include file="head.jsp" %>
    </head>
    <body class="max-height">
        <div class="grid-container">
            <div class="l-card l-card-big align_center">
                <h1><a href="index.do">MediaNet</a></h1>
                <h3>Administation adhérents</h3>
                

                <br/>
                <form action="admin-adherent.do" method="POST">
                    <div class="grid-row"><p class="offset-s-3 small-2">Nom : </p><input class="small-4" name="nom" type="text" placeholder="Doe"></div>
                    <div class="grid-row"><p class="offset-s-3 small-2">Prénom : </p><input class="small-4" name="prenom" type="text" placeholder="John"></div>
                    <div class="grid-row"><p class="offset-s-3 small-2">Mail : </p><input class="small-4" name="mail" type="email" placeholder="john.doe@gmail.com"></div>
                    <div class="grid-row"><p class="offset-s-3 small-2">Adresse : </p><input class="small-4" name="adresse" type="text" placeholder="5 rue du boulevard 54000 Nancy"></div>
                    <div class="grid-row" style="margin-top: 2em;"><input type="submit" class="button blue raised good-width" value="Ajouter"/></div>
                </form>
                
                <a href="admin-genre.do" class="button blue raised good-width" style="margin-top: 2em;">Annuler</a>
                
            </div>
        </div>
    
        <%@ include file="javascript.jsp" %>
    </body>
</html>
