<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <%@ include file="head.jsp" %>
    </head>
    <body class="max-height">
        <div class="grid-container">
            <div class="l-card l-card-big align_center">
                <h1><a href="index.do">MediaNet</a></h1>
                <h3>Retour</h3>
                <form name="search" action="summary-return.do" method="POST">
                    <div class="inputList">
                        
                    </div>
                    <div class="grid-row">
                        <a class="addInput" href="javascript:void(0)"><i class="fa fa-plus" style="font-size: 2em;"></i></a>
                    </div>
                    <div class="grid-row">
                        <input type="submit" class="button good-width raised blue" value="Valider"/>
                    </div>

                </form>
                <a href="index.do" class="button good-width raised blue" style="margin-top: 2em;">Retour à l'accueil</a>
            </div>
        </div>
        <%@ include file="javascript.jsp" %>
        <script>
            var count = 1;
            function addInput() {
                $('.inputList').first().append('<input name="document'+count+'" class="small-12 offset-m-3 medium-6" style="margin-bottom: 1em;" type="text" placeholder="Identifiant document" value=""/>');
                $('.inputList input').last().focus();
                count++;
            }
            addInput();
            $('.addInput').on('focus', function() {
                addInput();
            });
        </script>
    </body>
</html>
