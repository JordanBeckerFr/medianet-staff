<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <%@ include file="head.jsp" %>
    </head>
    <body class="max-height">
        <div class="grid-container">
            <div class="l-card l-card-big align_center">
                <h1><a href="index.do">MediaNet</a></h1>
                <h3 style="margin-top: 1em;">Recherche</h3>
                <form name="search" action="search.do" method="GET">
                    <div class="grid-row">
                        <input name="document" class="small-12 offset-m-3 medium-6" type="text" placeholder="Titre ou description" value=""/>
                    </div>
                    <div class="grid-row">
                        <select name="type" class="small-12 offset-m-3 medium-3" style="margin-bottom: 1em;">
                            <option value="" selected="selected">Tous</option>
                            <c:forEach var="i" items="${listeType}">
                                <option value="${i.id}">${i.nom}</option>
                            </c:forEach>
                        </select>
                        <select name="genre" class="small-12 medium-3">
                            <option value="" selected="selected">Tous</option>
                            <c:forEach var="i" items="${listeGenre}">
                                <option value="${i.id}">${i.nom}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="grid-row">
                        <input class="button good-width blue raised" type="submit" value="Rechercher"/>
                    </div>
                </form>
                
                
                <h3 style="margin-top: 2em;">Gestion</h3>
                
                <div class="grid-row">
                    <a href="borrow.do" class="button blue raised good-width">Emprunter</a>
                </div>
                
                <div class="grid-row">
                    <a href="return.do" class="button blue raised good-width">Retourner</a>
                </div>
                
                <h3 style="margin-top: 2em;">Profil Adhérent</h3>
                <c:if test="${!empty messageConnexion}">
                    <p style="margin-bottom: 1em;">${messageConnexion}</p>
                </c:if>
                <form name="account" action="account.do" method="POST">
                    <div class="grid-row">
                        <input name="adherent" class="small-12 offset-m-3 medium-6" type="text" placeholder="Numéro d'adhérent" value=""/>
                    </div>
                    <div class="grid-row">
                        <input type="submit" class="button good-width blue raised" style="margin-top: 1em;" value="Accéder"/>
                    </div>
                </form>
                
                <h3 style="margin-top: 2em;">Administration</h3>
                <div class="grid-row">
                    <a href="admin-document.do" class="button blue raised good-width">Gérer les documents</a>
                </div>
                <div class="grid-row">
                    <a href="admin-adherent.do" class="button blue raised good-width">Gérer les adhérents</a>
                </div>
                <div class="grid-row">
                    <a href="admin-genre.do" class="button blue raised good-width">Gérer les genres</a>
                </div>
            </div>
        </div>
        <%@ include file="javascript.jsp" %>
    </body>
</html>
