<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <%@ include file="head.jsp" %>
    </head>
    <body class="max-height">
        <div class="grid-container">
            <div class="l-card l-card-big align_center">
                <h1><a href="index.do">MediaNet</a></h1>
                <h3>Administation genres</h3>
                
                
                <div class="grid-row">
                    <table class="small-12 offset-m-2 medium-8">
                        <thead>
                            <tr>
                                <th style="width: 25%">Identifiant</th>
                                <th style="width: 75%">Nom</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="i" items="${listeGenre}">
                                <tr>
                                    <td>${i.id}</td>
                                    <td>${i.nom}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
                
                <a href="admin-add-genre.do" class="button blue raised good-width" style="margin-top: 2em;">Ajouter genre</a>
                
                <a href="index.do" class="button blue raised good-width" style="margin-top: 2em;">Retour à l'index</a>
            </div>
        </div>
    
        <%@ include file="javascript.jsp" %>
    </body>
</html>