<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <%@ include file="head.jsp" %>
    </head>
    <body class="max-height">
        <div class="grid-container">
            <div class="l-card l-card-big align_center">
                <h1><a href="index.do">MediaNet</a></h1>
                <h3>Administation documents</h3>
                

                <br/>
                <form action="admin-document.do" method="POST">
                    <div class="grid-row"><p class="offset-s-3 small-2">Titre : </p><input class="small-4" name="titre" type="text"></div>
                    <div class="grid-row"><p class="offset-s-3 small-2">Auteur : </p><input class="small-4" name="auteur" type="text"></div>
                    <div class="grid-row"><p class="offset-s-3 small-2">Description : </p><input class="small-4" name="description" type="text"></div>
                    <div class="grid-row"><p class="offset-s-3 small-2">URL image : </p><input class="small-4" name="url" type="text"></div>
                    <div class="grid-row"><p class="offset-s-3 small-2">Date ajout : : </p><input class="small-4" name="date-ajout" type="date"></div>
                    <div class="grid-row"><p class="offset-s-3 small-2">Date publication : </p><input class="small-4" name="date-publication" type="date"></div>
                    <div class="grid-row">
                        <p class="offset-s-3 small-2">Etat : </p>
                        <select class="small-4" name="etat">
                            <c:forEach var="i" items="${listeEtat}">
                                <option value="${i.id}">${i.nom}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="grid-row">
                        <p class="offset-s-3 small-2">Genre : </p>
                        <select class="small-4" name="genre">
                            <c:forEach var="i" items="${listeGenre}">
                                <option value="${i.id}">${i.nom}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="grid-row">
                        <p class="offset-s-3 small-2">Type : </p>
                        <select class="small-4" name="type">
                            <c:forEach var="i" items="${listeType}">
                                <option value="${i.id}">${i.nom}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="grid-row" style="margin-top: 2em;"><input type="submit" class="button blue raised good-width" value="Ajouter"/></div>
                </form>
                
                <a href="admin-genre.do" class="button blue raised good-width" style="margin-top: 2em;">Annuler</a>
                
            </div>
        </div>
    
        <%@ include file="javascript.jsp" %>
    </body>
</html>
