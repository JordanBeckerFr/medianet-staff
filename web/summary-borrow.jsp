<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <%@ include file="head.jsp" %>
    </head>
    <body class="max-height">
        <div class="grid-container">
            <div class="l-card l-card-big align_center">
                <h1><a href="index.do">MediaNet</a></h1>
                <h3>Liste des documents empruntés</h3>
                
                <c:choose>
                    <c:when test= "${nombreEmprunt gt 1}">
                        <h5 style="margin-top:2em;">${nombreEmprunt} documents empruntés au total</h5>
                    </c:when>
                    <c:when test= "${nombreEmprunt == 1}">
                        <h5 style="margin-top:2em;">1 documents empruntés au total</h5>
                    </c:when>
                    <c:otherwise>
                        <h5 style="margin-top:2em;">Aucun document emprunté actuellement</h5>
                    </c:otherwise>
                </c:choose>
                <div class="grid-row">
                    <table class="small-12 offset-m-2 medium-8">
                        <thead>
                            <tr>
                                <th style="width: 25%">Titre</th>
                                <th style="width: 25%">Auteur</th>
                                <th style="width: 25%">Date d'emprunt</th>
                                <th style="width: 25%">Date de retour</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="i" items="${listeEmprunt}">
                                <tr>
                                    <td>${i.key.documentId.titre}</td>
                                    <td>${i.key.documentId.auteur}</td>
                                    <td>${i.key.dateEmprunt}</td>
                                    <td>${i.value}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
                <div class="grid-row">
                    <form method="POST" action="summary.do" target="_blank">
                        <input type="hidden" name="adherent" value="${i.id}"/>
                        <input type="submit" class="button blue raised good-width" style="margin-top: 0.5em; margin-bottom: 0.5em;" value="Générer Récapitulatif"/>
                    </form>
                </div>
                <c:if test= "${!empty nombreErreur}">
                    <c:choose>
                        <c:when test= "${nombreErreur gt 1}">
                            <h5 style="margin-top:2em;">${nombreErreur} documents invalides</h5>
                        </c:when>
                        <c:otherwise>
                            <h5 style="margin-top:2em;">1 document invalide</h5>
                        </c:otherwise>
                    </c:choose>
                    <div class="grid-row">
                        <table class="small-12 offset-m-2 medium-8"> 
                            <tbody>
                                <c:forEach var="i" items="${listeInconnu}">
                                    <tr>
                                        <td>Identifiant invalide : ${i}</td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </c:if>
                <a href="index.do" class="button blue raised good-width" style="margin-top: 2em;">Retour à l'index</a>
            </div>
        </div>
        <%@ include file="javascript.jsp" %>
    </body>
</html>
