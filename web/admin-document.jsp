<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <%@ include file="head.jsp" %>
    </head>
    <body class="max-height">
        <div class="grid-container">
            <div class="l-card l-card-big align_center">
                <h1><a href="index.do">MediaNet</a></h1>
                <h3>Administation documents</h3>
                
                
                <div class="grid-row">
                    <table class="small-12 offset-m-2 medium-8">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Titre</th>
                                <th>Auteur</th>
                                <th>Genre</th>
                                <th>Type</th>
                                <th>Etat</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="i" items="${listeDocument}">
                                <tr>
                                    <td>${i.id}</td>
                                    <td>${i.titre}</td>
                                    <td>${i.auteur}</td>
                                    <td>${i.genreId.nom}</td>
                                    <td>${i.typeId.nom}</td>
                                    <td>${i.etatId.nom}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
                
                <a href="admin-add-document.do" class="button blue raised good-width" style="margin-top: 2em;">Ajouter document</a>
                
                <a href="index.do" class="button blue raised good-width" style="margin-top: 2em;">Retour à l'index</a>
            </div>
        </div>
    
        <%@ include file="javascript.jsp" %>
    </body>
</html>
