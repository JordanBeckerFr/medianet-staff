<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <%@ include file="head.jsp" %>
    </head>
    <body class="max-height">
        <div class="grid-container">
            <div class="l-card l-card-big align_center">
                <h1><a href="index.do">MediaNet</a></h1>

                <div class="grid-row">
                    <h3>Adhérents concernés</h3>
                </div>
                <div class="grid-row">
                    <h5>Adhérents concernés par cette session de retour</h5>
                </div>
                <div class="grid-row">
                    <table class="small-12 offset-m-2 medium-8">
                        <thead>
                            <tr>
                                <th style="width: 25%">Id</th>
                                <th style="width: 25%">Nom</th>
                                <th style="width: 25%">Prénom</th>
                                <th style="width: 25%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:if test="${empty adherents}">
                                <tr>
                                    <td colspan="4">Aucun adhérent concerné</td>
                                </tr>
                            </c:if>
                            <c:forEach var="i" items="${adherents}">
                                <tr>
                                    <td>${i.value.id}</td>
                                    <td>${i.value.nom}</td>
                                    <td>${i.value.prenom}</td>
                                    <td>
                                        <form method="POST" action="summary.do" target="_blank">
                                            <input type="hidden" name="adherent" value="${i.value.id}"/>
                                            <input type="submit" class="blue raised" style="margin-top: 0.5em; margin-bottom: 0.5em;" value="Générer Récapitulatif"/>
                                        </form>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>

                <div class="grid-row" style="margin-top: 2em;">
                    <h3>Liste des documents retournés</h3>
                </div>
                <c:choose>
                    <c:when test= "${nombreEmprunt gt 1}">
                        <div class="grid-row">
                            <h5>${nombreEmprunt} documents retournés avec succès</h5>
                        </div>
                    </c:when>
                    <c:when test= "${nombreEmprunt == 1}">
                        <div class="grid-row">
                            <h5>1 document retourné avec succès</h5>
                        </div>
                    </c:when>
                </c:choose>
                <div class="grid-row">
                    <table class="small-12 offset-m-2 medium-8">
                        <thead>
                            <tr>
                                <th style="width: 25%">Titre</th>
                                <th style="width: 25%">Auteur</th>
                                <th style="width: 25%">Date d'emprunt</th>
                                <th style="width: 25%">Date de retour</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:if test="${empty listeEmprunt}">
                                <tr>
                                    <td colspan="4">Aucun document retourné</td>
                                </tr>
                            </c:if>
                            <c:forEach var="i" items="${listeEmprunt}">
                                <tr>
                                    <td>${i.documentId.titre}</td>
                                    <td>${i.documentId.auteur}</td>
                                    <td>${i.dateEmprunt}</td>
                                    <td>${i.dateRetour}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>

                <c:if test= "${!empty nbErreurs}">
                    <div class="grid-row" style="margin-top: 3em;">
                        <c:choose>
                            <c:when test= "${nbErreurs gt 1}">
                                <h5>${nbErreurs} documents invalides</h5>
                            </c:when>
                            <c:otherwise>
                                <h5>1 document invalide</h5>
                            </c:otherwise>
                        </c:choose>
                        <table class="small-12 offset-m-2 medium-8"> 
                            <tbody>
                                <c:forEach var="i" items="${listeInconnu}">
                                    <tr>
                                        <td>Identifiant invalide : ${i}</td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </c:if>
                
                <a href="index.do" class="button blue raised good-width" style="margin-top: 2em;">Retour à l'index</a>
            </div>
        </div>
        <%@ include file="javascript.jsp" %>
    </body>
</html>
