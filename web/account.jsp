<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <%@ include file="head.jsp" %>
    </head>
    <body class="max-height">
        <div class="grid-container">
            <div class="l-card l-card-big align_center">
                <h1><a href="index.do">MediaNet</a></h1>
                <h4>${adherentObject.prenom} ${adherentObject.nom} (id: ${adherentObject.id})</h4>

                <h5 style="margin-top: 2em;">Documents à rendre</h5>
                <div class="grid-row">
                    <table class="small-12 offset-m-2 medium-8">
                        <thead>
                            <tr>
                                <th style="width: 25%">Titre</th>
                                <th style="width: 25%">Auteur</th>
                                <th style="width: 25%">Date d'emprunt</th>
                                <th style="width: 25%">Temps restant</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:if test="${empty listeEmprunt}">
                                <tr>
                                    <td colspan="4">Aucun document emprunté actuellement</td>
                                </tr>
                            </c:if>
                            <c:forEach var="i" items="${listeEmprunt}">
                                <tr>
                                    <td>${i.key.documentId.titre}</td>
                                    <td>${i.key.documentId.auteur}</td>
                                    <td>${i.key.dateEmprunt}</td>
                                    <td>${i.value}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
                <div class="grid-row">
                    <form action="summary.do" method="POST" target="_blank">
                        <input type="hidden" name="adherent" value="${adherentObject.id}"/>
                        <input type="submit" class="small-12 offset-m-4 medium-4 blue raised" style="margin-top: 1em;" value="Générer récapitulatif"/>
                    </form>
                </div>
                <h5 style="margin-top: 2em;">Documents rendus</h5>
                <div class="grid-row">
                    <table class="small-12 offset-m-2 medium-8">
                        <thead>
                            <tr>
                                <th style="width:25%">Titre</th>
                                <th style="width:25%">Auteur</th>
                                <th style="width:25%">Date d'emprunt</th>
                                <th style="width:25%">Date de retour</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:if test="${empty listeEmpruntRendu}">
                                <tr>
                                    <td colspan="4">Aucun document emprunté retourné</td>
                                </tr>
                            </c:if>
                            <c:forEach var="i" items="${listeEmpruntRendu}">
                                <tr>
                                    <td>${i.key.documentId.titre}</td>
                                    <td>${i.key.documentId.auteur}</td>
                                    <td>${i.key.dateEmprunt}</td>
                                    <td>${i.value}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
                <a href="index.do" class="button blue raised good-width" style="margin-top: 2em;">Retour à l'index</a>
            </div>
        </div>
    
        <%@ include file="javascript.jsp" %>
    </body>
</html>
