<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>MediaNet</title>
<link href="https://fonts.googleapis.com/css?family=RobotoDraft:100,200,300,400,500,700" rel="stylesheet" type="text/css">
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/framework.css" type="text/css">
<style>
    body {
        background: linear-gradient(160deg, rgb(60, 33, 89) 0%,rgb(30, 93, 118) 100%);
        background-attachment: fixed;
    }
    input[type=checkbox]{
        height: inherit;
    }
</style>
