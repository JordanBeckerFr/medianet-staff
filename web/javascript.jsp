<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script>
    (function($) {
        var height = $(window).height();
        sizeScreen();

        $(window).on('resize', function() {
            sizeScreen();
        });
        function sizeScreen() {
            height = $(window).height();
            $('.max-height').css({
                'height':height+'px'
            });
        }
    })(jQuery);

</script>