CREATE TABLE ADHERENT (
        ID INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
        NOM VARCHAR(255),
        PRENOM VARCHAR(255),
        MAIL VARCHAR(255),
        DATE_ADHESION DATE,
        ADRESSE VARCHAR(255),
        PRIMARY KEY (ID));

CREATE TABLE ETAT (
        ID INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
        NOM VARCHAR(255),
        DESCRIPTION VARCHAR(255),
        PRIMARY KEY (ID));

CREATE TABLE GENRE (
        ID INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
        NOM VARCHAR(255),
        PRIMARY KEY (ID));

CREATE TABLE TYPE (
        ID INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
        NOM VARCHAR(255),
        PRIMARY KEY (ID));

CREATE TABLE DOCUMENT (
        ID INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
        TITRE VARCHAR(255),
        AUTEUR VARCHAR(255),
        DESCRIPTION VARCHAR(255),
        URL VARCHAR(255),
        DATE_AJOUT DATE,
        DATE_PUBLICATION DATE,
        ETAT_ID INTEGER NOT NULL,
        GENRE_ID INTEGER NOT NULL,
        TYPE_ID INTEGER NOT NULL,
        PRIMARY KEY (ID),
        Foreign Key (ETAT_ID) REFERENCES ETAT(ID),
        Foreign Key (GENRE_ID) REFERENCES GENRE(ID),
        Foreign Key (TYPE_ID) REFERENCES TYPE(ID));

CREATE TABLE EMPRUNT (
        ID INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
        DATE_EMPRUNT DATE,
        DATE_RETOUR DATE,
        ADHERENT_ID INTEGER NOT NULL,
        DOCUMENT_ID INTEGER NOT NULL,
        PRIMARY KEY (ID),
        Foreign Key (ADHERENT_ID) REFERENCES ADHERENT(ID),
        Foreign Key (DOCUMENT_ID) REFERENCES DOCUMENT(ID));

INSERT INTO ADHERENT (NOM, PRENOM, MAIL, DATE_ADHESION, ADRESSE) 
        VALUES ('APPLESEED', 'John', 'john.appleseed@gmail.com', '2000-01-10', '1, Infinite Loop Cupertino, CA');
INSERT INTO ADHERENT (NOM, PRENOM, MAIL, DATE_ADHESION, ADRESSE) 
        VALUES ('DOE', 'John', 'johndoe@caramail.com', '2000-01-11', '21, Jump Street New York, NY');
INSERT INTO ADHERENT (NOM, PRENOM, MAIL, DATE_ADHESION, ADRESSE) 
        VALUES ('HOLMES', 'Sherlock', 'sherlock@scotlandyard.co.uk', '2012-05-12', '221B, Baker Street London, UK');
INSERT INTO ETAT (NOM) 
        VALUES ('Disponible');
INSERT INTO ETAT (NOM) 
        VALUES ('Emprunté');
INSERT INTO ETAT (NOM, DESCRIPTION) 
        VALUES ('Indisponible', 'K C');
INSERT INTO GENRE (NOM) 
        VALUES ('Romance');
INSERT INTO GENRE (NOM) 
        VALUES ('Comédie');
INSERT INTO GENRE (NOM) 
        VALUES ('Action');
INSERT INTO GENRE (NOM)
        VALUES ('Philosophique');
INSERT INTO GENRE (NOM)
        VALUES ('Pop');
INSERT INTO GENRE (NOM)
        VALUES ('Electro');
INSERT INTO GENRE (NOM)
        VALUES ('Classique');
INSERT INTO TYPE (NOM) 
        VALUES ('Livre');
INSERT INTO TYPE (NOM) 
        VALUES ('CD Audio');
INSERT INTO TYPE (NOM) 
        VALUES ('DVD Vidéo');

-- DVDs
INSERT INTO DOCUMENT (TITRE, AUTEUR, DESCRIPTION, URL, DATE_AJOUT, DATE_PUBLICATION, ETAT_ID, GENRE_ID, TYPE_ID) 
        VALUES ('Avengers: l’ère d’Ultron', 'Joss Whedon', 'When Tony Stark tries to jumpstart a dormant peacekeeping program, things go awry and it is up to The Avengers to stop the villainous Ultron from enacting his terrible plans.', 'http://recommandedmovies.com/wp-content/uploads/2013/11/THE-AVENGERS-2-AGE-OF-ULTRON-POSTER.jpg', '2014-01-11', '2014-01-02', 1, 3, 3);
INSERT INTO DOCUMENT (TITRE, AUTEUR, DESCRIPTION, URL, DATE_AJOUT, DATE_PUBLICATION, ETAT_ID, GENRE_ID, TYPE_ID) 
        VALUES ('Fury', 'David Ayer', 'April, 1945. As the Allies make their final push in the European Theatre, a battle-hardened army sergeant named Wardaddy commands a Sherman tank and his five-man crew on a deadly mission behind enemy lines...', 'http://uproxx.files.wordpress.com/2014/06/fury-poster.jpg?w=650', '2014-11-15', '2014-11-05', 1, 3, 3);
INSERT INTO DOCUMENT (TITRE, AUTEUR, DESCRIPTION, URL, DATE_AJOUT, DATE_PUBLICATION, ETAT_ID, GENRE_ID, TYPE_ID) 
        VALUES ('Interstellar', 'Christopher Nolan', 'A group of explorers make use of a newly discovered wormhole to surpass the limitations on human space travel and conquer the vast distances involved in an interstellar voyage.', 'http://media.comicbook.com/wp-content/uploads/2014/05/interstellar-poster.jpg', '2014-11-11', '2014-11-05', 1, 3, 3);

-- Livres
INSERT INTO DOCUMENT (TITRE, AUTEUR, DESCRIPTION, URL, DATE_AJOUT, DATE_PUBLICATION, ETAT_ID, GENRE_ID, TYPE_ID) 
        VALUES ('Et rien d’autre',
                'Salter James',
                'La Seconde Guerre mondiale vit ses derniers instants. Sur un porte-avions au large du Japon, le jeune officier Philip Bowman rentre à New York...',
                'http://images.telerama.fr//medias/2014/08/media_115799/et-rien-d-autre,M164338.jpg',
                '2014-08-25',
                '2014-08-21',
                1,
                1,
                1);
INSERT INTO DOCUMENT (TITRE, AUTEUR, DESCRIPTION, URL, DATE_AJOUT, DATE_PUBLICATION, ETAT_ID, GENRE_ID, TYPE_ID) 
        VALUES ('L’attrape-coeurs',
                'Jerome David Salinger', 
                'Phénomène littéraire sans équivalent depuis les années 50, J. D. Salinger reste le plus mystérieux des écrivains contemporains, et son chef-d’œuvre, "L’attrape-cœurs", est le roman de l’adolescence le plus lu du monde entier.',
                'http://ecx.images-amazon.com/images/I/51FV05TVM3L.jpg',
                '2014-05-27',
                '2014-05-27',
                1,
                1,
                1);
INSERT INTO DOCUMENT (TITRE, AUTEUR, DESCRIPTION, URL, DATE_AJOUT, DATE_PUBLICATION, ETAT_ID, GENRE_ID, TYPE_ID) 
        VALUES ('Des souris et des hommes', 
                'John Steinbeck', 
                'Les deux hommes levèrent les yeux car le rectangle de soleil de la porte s’était masqué. Debout, une jeune femme regardait dans la chambre...',
                'http://www.images-booknode.com/book_cover/0/full/des-souris-et-des-hommes-184.jpg',
                '2012-09-08', 
                '1972-02-16', 
                1, 
                4, 
                1);

-- CDs
INSERT INTO DOCUMENT (TITRE, AUTEUR, DESCRIPTION, URL, DATE_AJOUT, DATE_PUBLICATION, ETAT_ID, GENRE_ID, TYPE_ID) 
        VALUES ('Entity', 
                'Oscar And The Wolf', 
                'L’électro d’Oscar & the Wolf est très visuelle et évocatrice. Entity nous fait voyager dans un univers onirique, aérien, rempli d’images et de couleurs, ce qui est loin d’être désagréable.', 
                'http://static.fnac-static.com/multimedia/Images/FR/NR/a5/a4/5d/6136997/1507-1.jpg', 
                '2014-11-05', 
                '2014-10-20', 
                1, 
                6, 
                2);


INSERT INTO DOCUMENT (TITRE, AUTEUR, DESCRIPTION, URL, DATE_AJOUT, DATE_PUBLICATION, ETAT_ID, GENRE_ID, TYPE_ID) 
        VALUES ('Babel', 
                'Jean-Louis Murat', 
                'Cette fois-ci Jean-Louis Murat ne fait pas cavalier seul. C’est en compagnie du Delano Orchestra qu’il nous livre ses poèmes durs ou tendres, qui sentent bon le vent, la terre et l’herbe coupée', 
                'http://static.fnac-static.com/multimedia/Images/FR/NR/ec/16/5f/6231788/1507-1.jpg', 
                '2014-11-05', 
                '2014-10-13', 
                1, 
                7, 
                2);
INSERT INTO DOCUMENT (TITRE, AUTEUR, DESCRIPTION, URL, DATE_AJOUT, DATE_PUBLICATION, ETAT_ID, GENRE_ID, TYPE_ID) 
        VALUES ('Vs Head Vs Heart', 
                'Emma Louise', 
                'Une électro pop efficace servant d’écrin à une voix cristalline, voici le 1er album d’une jeune songwriter australienne, qui part à la conquête du monde après des débuts remarqués sur son continent.', 
                'http://static.fnac-static.com/multimedia/Images/FR/NR/82/50/4e/5132418/1507-1.jpg', 
                '2014-11-10', 
                '2014-11-10', 
                1, 
                5, 
                2);