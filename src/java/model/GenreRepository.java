package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class GenreRepository {
    
    private ArrayList<Genre> genreList;
    
    /**
     * Retourne la liste de tous les genre
     * @param conn
     * @return ArrayList>Genre>
     */
    public ArrayList<Genre> getGenreList(Connection conn) {
        genreList = new ArrayList<>();
        
        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM GENRE");
            ResultSet rs = stmt.executeQuery();
            while(rs.next()) {
                Genre tmp = new Genre(rs.getInt("ID"), rs.getString("NOM"));
                genreList.add(tmp);
            }
        } catch (Exception e) {
            System.err.println(e);
        }
        
        return genreList;
    }
    
    /**
     * Retourne le genre possedant l'id passe en parametre
     * @param conn
     * @param id
     * @return Genre
     */
    public Genre getGenreById(Connection conn,String id){
        Genre genre= null;
        try{
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM GENRE WHERE ID = ?");
            stmt.setInt(1,Integer.parseInt(id));
            
            ResultSet rs = stmt.executeQuery();
            rs.next();
            genre = new Genre(rs.getInt("ID"),rs.getString("NOM"));
        } catch (Exception ex) {
            System.err.println(ex);
        }
        return genre;
    }
    
    /**
     * Ajoute un genre dans la base de donnees en passant le nom en parametre
     * @param conn
     * @param nom 
     */
    public void addGenre(Connection conn, String nom) {
        String query = "INSERT INTO GENRE (NOM) VALUES (?)";
        
        try {
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setString(1, nom);
            stmt.executeUpdate(); 
        } catch (SQLException e) {
            System.err.println(e);
        }   
    }
}
