package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class EtatRepository {
    
    private ArrayList<Etat> etatList;
    
    private Connection conn;
    
    /**
     * Retourne la liste de tous les etats
     * @param conn
     * @return ArrayList<Etat>
     */
    public ArrayList<Etat> getEtatList(Connection conn) {
        etatList = new ArrayList<>();
        
        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM ETAT");
            ResultSet rs = stmt.executeQuery();
            while(rs.next()) {
                Etat tmp = new Etat(rs.getInt("ID"), rs.getString("NOM"), rs.getString("DESCRIPTION"));
                etatList.add(tmp);
            }
        } catch (Exception e) {
            System.err.println(e);
        }
        
        return etatList;
    }
    
    /**
     * Retourne l'etat possedant l'id passe en parametre
     * @param conn
     * @param id
     * @return Etat
     */
    public Etat getEtatById(Connection conn,String id){
        Etat etat = null;
        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM ETAT WHERE ID = ? ");
            stmt.setInt(1, Integer.parseInt(id));
            
            ResultSet rs = stmt.executeQuery();
            rs.next();
            etat = new Etat(rs.getInt("ID"),rs.getString("NOM"),rs.getString("DESCRIPTION"));
        } catch (Exception ex) {
            System.err.println(ex);
        }
        return etat;
    }
}
