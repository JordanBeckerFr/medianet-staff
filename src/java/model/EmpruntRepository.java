package model;

import exception.UnknownAdherentException;
import exception.UnknownDocumentException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class EmpruntRepository {
    
    private ArrayList<Emprunt> empruntList;
    
    /**
     * Retourne une liste d'emprunts
     * @param conn
     * @return 
     */
    public ArrayList<Emprunt> getEmpruntList(Connection conn) {
        empruntList = new ArrayList<>();
        
        try {
            DocumentRepository documentRepository = new DocumentRepository();
            AdherentRepository adherentRepository = new AdherentRepository();
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM EMPRUNT");
            ResultSet rs = stmt.executeQuery();
            while(rs.next()) {
                Document document = documentRepository.getDocumentById(conn, Integer.toString(rs.getInt("DOCUMENT_ID")));
                Adherent adherent = adherentRepository.getAdherentById(conn, Integer.toString(rs.getInt("ADHERENT_ID")));
                Emprunt tmp = new Emprunt(rs.getInt("ID"), rs.getDate("DATE_EMPRUNT"), rs.getDate("DATE_RETOUR"), adherent, document);
                empruntList.add(tmp);
            }
        } catch (Exception e) {
            System.err.println(e);
        }
        
        return empruntList;
    }
    
    /**
     * Retour une liste d'emprunt par un adhérent
     * @param conn
     * @param id_Adherent
     * @return 
     */
    public ArrayList<Emprunt> getEmpruntListByAdherent(Connection conn,String id_Adherent) {
        empruntList = new ArrayList<>();
        try {
            DocumentRepository documentRepository = new DocumentRepository();
            AdherentRepository adherentRepository = new AdherentRepository();
            
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM EMPRUNT WHERE ADHERENT_ID = ?");
            stmt.setInt(1, Integer.parseInt(id_Adherent));
            ResultSet rs = stmt.executeQuery();
            while(rs.next()) {
                Document document = documentRepository.getDocumentById(conn, Integer.toString(rs.getInt("DOCUMENT_ID")));
                Adherent adherent = adherentRepository.getAdherentById(conn, Integer.toString(rs.getInt("ADHERENT_ID")));
                Emprunt emprunt = new Emprunt(rs.getInt("ID"), rs.getDate("DATE_EMPRUNT"), rs.getDate("DATE_RETOUR"), adherent, document);
                
                empruntList.add(emprunt);
            }
        } catch (SQLException e) {
            System.err.println(e);
        }
        
        return empruntList;
        
    }
    
    /**
     * Retourne une liste d'emprunts possédant l'état souhaité
     * @param conn
     * @param list
     * @param id_Etat
     * @return 
     */
    public ArrayList<Emprunt> getEmpruntListTriee(Connection conn,ArrayList<Emprunt> list,int id_Etat){
        empruntList = new ArrayList<>();
        DocumentRepository documentRepository = new DocumentRepository();
        for(Emprunt emprunt : list){
            Document document = emprunt.getDocumentId();
            if(document.getEtatId().getId() == id_Etat) {
                empruntList.add(emprunt);
            }
        }
        return empruntList;
    }
    
    /**
     * Retourne une liste d'emprunts avec une liste de paramètres
     * @param conn
     * @param parameters
     * @return 
     */
    public ArrayList<Emprunt> getEmpruntListByParam(Connection conn, HashMap<String, String> parameters) {
        empruntList = new ArrayList<>();
        
        HashMap<String, String> paramListe = new HashMap<>();
        
        //Construction d'une requete SQL depuis la liste de paramètres
        String[] nomFiltre = new String[] {"id", "adherent", "document", "retourne", "non-retourne"};
        for(String str : nomFiltre) {
            if(parameters.containsKey(str)) {
                if(parameters.get(str).length() > 0) {
                    paramListe.put(str, parameters.get(str));
                }
            }
        }
        
        try {
            
            String query = "SELECT * FROM EMPRUNT";
            int count = 0;
            for(String key : paramListe.keySet()) {
                
                if(count == 0) {
                    query += " WHERE ";
                } else {
                    query += " AND ";
                }
                
                switch (key) {
                    case "id":
                        query += "id = ?";
                        break;
                    case "adherent":
                        query += "adherent_id = ?";
                        break;
                    case "document":
                        query += "document_id = ?";
                        break;
                    case "retourne":
                        query += "date_retour IS NOT NULL";
                        break;
                    case "non-retourne":
                        query += "date_retour IS NULL";
                        break;
                    default:
                        break;
                }
                count++;
            }
                
            PreparedStatement stmt = conn.prepareStatement(query);
            
            count = 1;
            try {
                for(String key : paramListe.keySet()) {
                    switch (key) {
                        case "id":
                            stmt.setInt(count, Integer.parseInt(paramListe.get(key)));
                            break;
                        case "adherent":
                            stmt.setInt(count, Integer.parseInt(paramListe.get(key)));
                            break;
                        case "document":
                            stmt.setInt(count, Integer.parseInt(paramListe.get(key)));
                        case "retourne":
                        case "non-retourne":
                            count--;
                            break;
                        default:
                            break;
                    }
                    count++;
                }
            } catch (NumberFormatException e) {
                return null;
            }
            
            stmt.executeQuery();
            ResultSet rs = stmt.executeQuery();
            
            AdherentRepository adherentRepository=new AdherentRepository();
            DocumentRepository documentRepository=new DocumentRepository();
            
            while(rs.next()){
                Adherent adherent = adherentRepository.getAdherentById(conn, Integer.toString(rs.getInt("ADHERENT_ID")));
                Document document = documentRepository.getDocumentById(conn, Integer.toString(rs.getInt("DOCUMENT_ID")));
                Emprunt emprunt = new Emprunt(rs.getInt("ID"), rs.getDate("DATE_EMPRUNT"), rs.getDate("DATE_RETOUR"), adherent, document);
                empruntList.add(emprunt);
            }
            
        } catch (Exception e) {
            System.err.println(e);
        }
        
        return empruntList;
    }
    
    public ArrayList<Emprunt> insertEmprunts(Connection conn, String adherentId,ArrayList<Integer> documents) throws UnknownAdherentException, UnknownDocumentException{
        // Verif adhérent
        AdherentRepository adherentRepository = new AdherentRepository();
        Adherent adherent = adherentRepository.getAdherentById(conn, adherentId);
        if(adherent == null) {
            throw new UnknownAdherentException();
        }
        
        DocumentRepository documentRepository = new DocumentRepository();
        
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd");
        Date date = new Date();
        String date_s = sdf.format(date);
        String query = "INSERT INTO EMPRUNT (DATE_EMPRUNT,ADHERENT_ID,DOCUMENT_ID) values (?,?,?)";
        
        int id=0;
        ArrayList<Integer> documents_inconnus=new ArrayList<>();
        for(Integer i : documents) {
            try {
                Document document = documentRepository.getDocumentById(conn, Integer.toString(i));
                if ( document != null && document.getEtatId().getId() == 1){
                        
                    PreparedStatement stmt = conn.prepareStatement(query,PreparedStatement.RETURN_GENERATED_KEYS);
                    stmt.setString(1, date_s);
                    stmt.setInt(2, Integer.parseInt(adherentId));
                    stmt.setInt(3, i);
                    stmt.executeUpdate();
                    ResultSet rs =stmt.getGeneratedKeys();
                    rs.next();
                    documentRepository.updateEtat(conn, i, Etat.EMPRUNTE);
                } else {
                    documents_inconnus.add(i);
                }
            } catch (SQLException ex) {
                Logger.getLogger(EmpruntRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        //Retourne la liste de tous les emprunts de l'adhérent
        HashMap<String, String> params = new HashMap<>();
        params.put("adherent", adherentId);
        params.put("non-retourne", "1");
        ArrayList<Emprunt> list = getEmpruntListByParam(conn, params);
        
        if(!documents_inconnus.isEmpty()){
            throw new UnknownDocumentException(documents_inconnus,list);
        }
        return list;
    }
    
    public Emprunt getEmpruntById(Connection conn, String id){
        Emprunt emprunt = null;
        try {
            AdherentRepository adherentRepository = new AdherentRepository();
            DocumentRepository documentRepository = new DocumentRepository();
            String query = "SELECT * FROM EMPRUNT WHERE ID = ?";
            
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setInt(1, Integer.parseInt(id));
            
            ResultSet rs = stmt.executeQuery();
            rs.next();
            Adherent adherent = adherentRepository.getAdherentById(conn, Integer.toString(rs.getInt("ADHERENT_ID")));
            Document document = documentRepository.getDocumentById(conn, Integer.toString(rs.getInt("DOCUMENT_ID")));
            
            emprunt = new Emprunt(rs.getInt("ID"), rs.getDate("DATE_EMPRUNT"), rs.getDate("DATE_RETOUR"), adherent, document);
        } catch (SQLException e) {
            System.err.println(e);
            return null;
        }
        
        return emprunt;
    }
    
    public ArrayList<Emprunt> returnDocuments(Connection conn, ArrayList<String> idDocuments) throws UnknownDocumentException{
        DocumentRepository documentRepository = new DocumentRepository();
        EmpruntRepository empruntRepository = new EmpruntRepository();
                
        ArrayList<Emprunt> listeEmprunt = new ArrayList<>();
        ArrayList<Integer> errorList = new ArrayList<>();
        
        //Chercher liste documents associés
        for (String idDocument : idDocuments) {
            if(idDocument.trim().length() > 0) {
                Document document = documentRepository.getDocumentById(conn, idDocument.trim());
                //Vérifie que le document existe
                if (document != null) {
                    if (document.getEtatId().getId() == Etat.EMPRUNTE) {
                        //On récupère l'emprunt correspondant au document passé dans la boucle
                        HashMap<String, String> parameters = new HashMap<>();
                        parameters.put("document", document.getId().toString());
                        parameters.put("non-retourne", "1");
                        ArrayList<Emprunt> emprunts = this.getEmpruntListByParam(conn, parameters);
                        Emprunt emprunt = emprunts.get(0);

                        //Mettre à jour l'emprunt pour lui ajouter la date de rendu
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY-MM-dd");
                        Date date = new Date();
                        String dateFormated = simpleDateFormat.format(date);
                        String query = "UPDATE EMPRUNT SET DATE_RETOUR = ? WHERE ID = ?";
                        try {
                            PreparedStatement stmt=conn.prepareStatement(query);
                            stmt.setString(1, dateFormated);
                            stmt.setInt(2, emprunt.getId());
                            stmt.executeUpdate();
                        } catch (SQLException ex) {
                            Logger.getLogger(EmpruntRepository.class.getName()).log(Level.SEVERE, null, ex);
                        }

                        //Mettre à jour l'état du document à disponible
                        documentRepository.updateEtat(conn, Integer.parseInt(idDocument), Etat.DISPONIBLE); 
                        
                        listeEmprunt.add(empruntRepository.getEmpruntById(conn, Integer.toString(emprunt.getId())));
                    }  else {
                        errorList.add(Integer.parseInt(idDocument));
                    }
                } else {
                    errorList.add(Integer.parseInt(idDocument));
                }
            }
        }
        
        if(!errorList.isEmpty()) {
            throw new UnknownDocumentException(errorList, listeEmprunt);
        }
        
        
        //Retourner la liste
        return listeEmprunt;
    }
    
}
