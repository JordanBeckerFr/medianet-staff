package model;

import java.util.Collection;
import java.util.Date;

public class Adherent {
    
    private Integer id;
    
    private String nom;
    
    private String prenom;
    
    private String mail;
    
    private Date dateAdhesion;
    
    private String adresse;
    
    private Collection<Emprunt> empruntCollection;

    public Adherent() {
    }

    public Adherent(Integer id, String nom, String prenom, String mail, Date dateAdhesion, String adresse) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.mail = mail;
        this.dateAdhesion = dateAdhesion;
        this.adresse = adresse;
    }

    public Adherent(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public Date getDateAdhesion() {
        return dateAdhesion;
    }

    public void setDateAdhesion(Date dateAdhesion) {
        this.dateAdhesion = dateAdhesion;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public Collection<Emprunt> getEmpruntCollection() {
        return empruntCollection;
    }

    public void setEmpruntCollection(Collection<Emprunt> empruntCollection) {
        this.empruntCollection = empruntCollection;
    }

    @Override
    public String toString() {
        return "Adherent{" + "id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", mail=" + mail + ", dateAdhesion=" + dateAdhesion + ", adresse=" + adresse + ", empruntCollection=" + empruntCollection + '}';
    }
    
}
