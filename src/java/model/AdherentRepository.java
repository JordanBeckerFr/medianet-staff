package model;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class AdherentRepository {
    
    private ArrayList<Adherent> adherentList;

    public AdherentRepository() {
    }
    
    /**
     * Retourne la liste des adhérents
     * @param conn
     * @return 
     */
    public ArrayList<Adherent> getAdherentList(Connection conn) {
        
        adherentList = new ArrayList<>();
        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM ADHERENT");
            ResultSet rs = stmt.executeQuery();
            while(rs.next()) {
                Adherent tmp = new Adherent(rs.getInt("ID"), rs.getString("NOM"), rs.getString("PRENOM"), rs.getString("MAIL"), rs.getDate("DATE_ADHESION"), rs.getString("ADRESSE"));
                adherentList.add(tmp);
            }
        } catch (Exception e) {
            System.out.println("Erreur adherent liste");
        }
        
        return adherentList;
    }
    
    /**
     * Retourne un adhérent pour un id
     * @param conn
     * @param id
     * @return 
     */
    public Adherent getAdherentById(Connection conn, String id) {
        Adherent adherent = null;
        try {
            String query = "SELECT * FROM ADHERENT WHERE ID = ?";
            
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setInt(1, Integer.parseInt(id));
            
            ResultSet rs = stmt.executeQuery();
            rs.next();
            adherent = new Adherent(rs.getInt("ID"), rs.getString("NOM"), rs.getString("PRENOM"), rs.getString("MAIL"), rs.getDate("DATE_ADHESION"), rs.getString("ADRESSE"));
        } catch (NumberFormatException e) {
            System.out.println("Erreur adherent by id");
            return null;
        } catch (SQLException e) {
            System.out.println("Erreur adherent SQLException");
            return null;
        }
        
        return adherent;
    }
    
    /**
     * Ajoute un adhérent
     * @param conn
     * @param parameters 
     */
    public void addAdherent(Connection conn, HashMap<String, String> parameters) {
        String query = "INSERT INTO ADHERENT (NOM, PRENOM, MAIL, DATE_ADHESION, ADRESSE) VALUES (?, ?, ?, ?, ?)";
        
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd");
        java.util.Date date = new java.util.Date();
        String dateFormated = sdf.format(date);
        
        try {
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setString(1, parameters.get("nom"));
            stmt.setString(2, parameters.get("prenom"));
            stmt.setString(3, parameters.get("mail"));
            stmt.setString(4, dateFormated);
            stmt.setString(5, parameters.get("adresse"));
            stmt.executeUpdate(); 
        } catch (SQLException e) {
            System.err.println(e);
        }   
    }
    
}
