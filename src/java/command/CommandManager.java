package command;

import java.util.HashMap;

public class CommandManager {

    public CommandManager() {
        init();
    }
    
    HashMap<String, Command> cmds;

    public void init() {
        this.cmds = new HashMap<>();
        this.cmds.put("index", new IndexCommand());
        this.cmds.put("search", new SearchCommand());
        this.cmds.put("detail", new DetailCommand());
        this.cmds.put("summary", new SummaryCommand());
        this.cmds.put("borrow", new BorrowCommand());
        this.cmds.put("return", new ReturnCommand());
        this.cmds.put("summary-borrow", new SummaryBorrowCommand());
        this.cmds.put("summary-return", new SummaryReturnCommand());
        this.cmds.put("account", new AccountCommand());
        this.cmds.put("admin-adherent", new AdminAdherentCommand());
        this.cmds.put("admin-genre", new AdminGenreCommand());
        this.cmds.put("admin-add-adherent", new AdminAddAdherentCommand());
        this.cmds.put("admin-add-genre", new AdminAddGenreCommand());
        this.cmds.put("admin-document", new AdminDocumentCommand());
        this.cmds.put("admin-add-document", new AdminAddDocumentCommand());
    }
    
    public Command getCommand(String cmd) {
//        return null;
        return this.cmds.get(cmd);
    }
}
