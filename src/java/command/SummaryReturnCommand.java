package command;

import exception.UnknownDocumentException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Adherent;
import model.Emprunt;
import model.EmpruntRepository;
import utils.DateConverter;

public class SummaryReturnCommand extends Command {

    private EmpruntRepository empruntRepository;
    
    @Override
    public String getCommandName() {
        return "summary-return";
    }
    
    @Override
    public ActionFlow execute(HashMap<String, String> parameters, HashMap<String, Object> configurations) {
        String vue = "summary-return";
        ServletContext servletContext = (ServletContext) configurations.get("servletContext");
        HttpServletRequest req = (HttpServletRequest) configurations.get("request");
        HttpServletResponse res = (HttpServletResponse) configurations.get("response");
        
        empruntRepository = new EmpruntRepository();
        
        //Parse la liste d'inputs du return.do pour gérer le retour multiple
        ArrayList<String> listeDocuments = new ArrayList<>();
        int i = 1;
        while(parameters.get(documentCount(i)) != null) {
            listeDocuments.add(parameters.get(documentCount(i)));
            i++;
        }
        
        ArrayList<Emprunt> emprunts = null;
        HashMap<Integer, Adherent> adherents = new HashMap<>();
        
        //Retourne deux listes s'il y a une erreur dans l'emprunt de document : emprunts et listeInconnu
        try {
            emprunts = empruntRepository.returnDocuments((Connection)servletContext.getAttribute("conn"), listeDocuments);
        } catch (UnknownDocumentException ex) {
            emprunts = ex.getListe_emprunts();
            req.setAttribute("listeInconnu", ex.getDocument_inconnus());
            req.setAttribute("message", ex.getMessage());
            req.setAttribute("nbErreurs", ex.getDocument_inconnus().size());
        }
        
        for (Emprunt emprunt : emprunts) {
            if (!adherents.containsKey(emprunt.getAdherentId().getId())) {
                adherents.put(emprunt.getAdherentId().getId(),emprunt.getAdherentId());
            } 
        }
        
        if(emprunts != null) {
            req.setAttribute("nombreEmprunt", emprunts.size());
        } else {
            req.setAttribute("nombreEmprunt", "0");
        }

        req.setAttribute("listeEmprunt", emprunts);
        req.setAttribute("adherents", adherents);
        
        return new ActionFlow(vue, vue+".jsp", false);
    }
    
    private String documentCount(int i) {
        return "document"+Integer.toString(i);
    }

}
