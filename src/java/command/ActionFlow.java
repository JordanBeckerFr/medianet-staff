package command;

public class ActionFlow {
    
    private String nom;
    private String path;
    private boolean redirect;

    public ActionFlow(String nom, String path, boolean redirect) {
        this.nom = nom;
        this.path = path;
        this.redirect = redirect;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean isRedirect() {
        return redirect;
    }

    public void setRedirect(boolean redirect) {
        this.redirect = redirect;
    }
    
}
