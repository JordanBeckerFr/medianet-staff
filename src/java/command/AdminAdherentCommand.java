package command;

import java.sql.Connection;
import java.util.HashMap;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import model.AdherentRepository;
import model.Document;
import model.DocumentRepository;
import model.GenreRepository;
import model.TypeRepository;

public class AdminAdherentCommand extends Command {

    private AdherentRepository adherentRepository;

    @Override
    public String getCommandName() {
        return "admin-adherent";
    }
    
    @Override
    public ActionFlow execute(HashMap<String, String> parameters, HashMap<String, Object> configurations) {
        String vue = "admin-adherent";
        
        ServletContext servletContext = (ServletContext) configurations.get("servletContext");
        HttpServletRequest req = (HttpServletRequest) configurations.get("request");
        
        adherentRepository = new AdherentRepository();
        
        if(parameters.get("nom") != null) {
            adherentRepository.addAdherent((Connection)servletContext.getAttribute("conn"), parameters);
        }
        
        req.setAttribute("listeAdherent", adherentRepository.getAdherentList((Connection)servletContext.getAttribute("conn")));
        
        return new ActionFlow(vue, vue+".jsp", false);
    }

}
