package command;

import java.sql.Connection;
import java.util.HashMap;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.GenreRepository;
import model.TypeRepository;

public class BorrowCommand extends Command {
    
    private TypeRepository typeRepository;
    private GenreRepository genreRepository;
    
    @Override
    public String getCommandName() {
        return "borrow";
    }
    
    @Override
    public ActionFlow execute(HashMap<String, String> parameters, HashMap<String, Object> configurations) {
        String vue = "borrow";
        ServletContext servletContext = (ServletContext) configurations.get("servletContext");
        HttpServletRequest req = (HttpServletRequest) configurations.get("request");
        HttpServletResponse res = (HttpServletResponse) configurations.get("response");
        
        typeRepository = new TypeRepository();
        genreRepository = new GenreRepository();
        
        req.setAttribute("listeType", typeRepository.getTypeList((Connection)servletContext.getAttribute("conn")));
        req.setAttribute("listeGenre", genreRepository.getGenreList((Connection)servletContext.getAttribute("conn")));
        
        return new ActionFlow(vue, vue+".jsp", false);
    }

}
