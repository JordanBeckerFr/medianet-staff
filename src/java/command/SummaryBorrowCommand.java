/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package command;

import exception.UnknownAdherentException;
import exception.UnknownDocumentException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import model.Emprunt;
import model.EmpruntRepository;
import utils.DateConverter;

/**
 *
 * @author database
 */
public class SummaryBorrowCommand extends Command{
    
    private EmpruntRepository empruntRepository;
    
    @Override
    public String getCommandName() {
        return "summary-borrow";
    }

    @Override
    public ActionFlow execute(HashMap<String, String> parameters, HashMap<String, Object> configurations) {
        
            String vue = "summary-borrow";
            
            ServletContext servletContext = (ServletContext) configurations.get("servletContext");
            HttpServletRequest req = (HttpServletRequest) configurations.get("request");
            empruntRepository = new EmpruntRepository();
            ArrayList<Emprunt> emprunts;
           
            //Parse la liste d'inputs du borrow.do pour gérer l'emprunt multiple
            ArrayList<Integer> listeDocuments = new ArrayList<>();
            int i = 1;
            while(parameters.get(documentCount(i)) != null) {
                int id_document = Integer.parseInt(parameters.get(documentCount(i)));
                listeDocuments.add(id_document);
                i++;
            }
            
            //Vérification si l'adhérent peut emprunter le ou les document
            try {
                emprunts = empruntRepository.insertEmprunts((Connection)servletContext.getAttribute("conn"), parameters.get("adherent"), listeDocuments);
                req.setAttribute("listeEmprunt", emprunts);
            } catch (UnknownAdherentException ex) {
                req.setAttribute("message", "L'adhérent n'existe pas");
                return new ActionFlow("erreur","borrow.jsp",false);
            } catch (UnknownDocumentException ex) {
                req.setAttribute("message", ex.getMessage());
                req.setAttribute("listeInconnu", ex.getDocument_inconnus());
                req.setAttribute("listeEmprunt", ex.getListe_emprunts());
                req.setAttribute("nombreErreur", ex.getDocument_inconnus().size());
                emprunts = ex.getListe_emprunts();
            } 
            
            HashMap<Emprunt, String> listeEmprunt = DateConverter.generateHashMap(emprunts);
            req.setAttribute("listeEmprunt", listeEmprunt);
            
            req.setAttribute("adherent", parameters.get("adherent"));
            req.setAttribute("nombreEmprunt",listeEmprunt.size());
            
            parameters.put("emprunt", "1");
            
            return new ActionFlow(vue, vue+".jsp", false);
        
    }
    
    private String documentCount(int i) {
        return "document"+Integer.toString(i);
    }
}
