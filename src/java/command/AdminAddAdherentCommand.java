package command;

import java.sql.Connection;
import java.util.HashMap;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import model.AdherentRepository;
import model.Document;
import model.DocumentRepository;
import model.GenreRepository;
import model.TypeRepository;

public class AdminAddAdherentCommand extends Command {

    private AdherentRepository adherentRepository;

    @Override
    public String getCommandName() {
        return "admin-add-adherent";
    }
    
    @Override
    public ActionFlow execute(HashMap<String, String> parameters, HashMap<String, Object> configurations) {
        String vue = "admin-add-adherent";
        
        ServletContext servletContext = (ServletContext) configurations.get("servletContext");
        HttpServletRequest req = (HttpServletRequest) configurations.get("request");
        
        return new ActionFlow(vue, vue+".jsp", false);
    }

}
