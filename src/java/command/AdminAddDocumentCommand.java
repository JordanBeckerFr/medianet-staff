package command;

import java.sql.Connection;
import java.util.HashMap;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.AdherentRepository;
import model.Document;
import model.DocumentRepository;
import model.EtatRepository;
import model.GenreRepository;
import model.TypeRepository;

public class AdminAddDocumentCommand extends Command {

    private DocumentRepository documentRepository;
    private GenreRepository genreRepository;
    private TypeRepository typeRepository;
    private EtatRepository etatRepository;
    

    @Override
    public String getCommandName() {
        return "admin-add-document";
    }
    
    @Override
    public ActionFlow execute(HashMap<String, String> parameters, HashMap<String, Object> configurations) {
        String vue = "admin-add-document";
        
        ServletContext servletContext = (ServletContext) configurations.get("servletContext");
        HttpServletRequest req = (HttpServletRequest) configurations.get("request");
        HttpServletResponse res = (HttpServletResponse) configurations.get("response");
        
        
        genreRepository = new GenreRepository();
        typeRepository = new TypeRepository();
        etatRepository = new EtatRepository();
        
        req.setAttribute("listeType", typeRepository.getTypeList((Connection)servletContext.getAttribute("conn")));
        req.setAttribute("listeEtat", etatRepository.getEtatList((Connection)servletContext.getAttribute("conn")));
        req.setAttribute("listeGenre", genreRepository.getGenreList((Connection)servletContext.getAttribute("conn")));
        
        return new ActionFlow(vue, vue+".jsp", false);
    }

}
