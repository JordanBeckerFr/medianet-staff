package command;

import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public abstract class Command {

    public abstract String getCommandName();
    public abstract ActionFlow execute(HashMap<String, String> parameters, HashMap<String, Object> configurations);
}
