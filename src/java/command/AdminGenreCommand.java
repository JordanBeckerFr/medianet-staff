package command;

import java.sql.Connection;
import java.util.HashMap;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import model.Document;
import model.DocumentRepository;
import model.GenreRepository;
import model.TypeRepository;

public class AdminGenreCommand extends Command {

    private GenreRepository genreRepository;

    @Override
    public String getCommandName() {
        return "admin-genre";
    }
    
    @Override
    public ActionFlow execute(HashMap<String, String> parameters, HashMap<String, Object> configurations) {
        String vue = "admin-genre";
        
        ServletContext servletContext = (ServletContext) configurations.get("servletContext");
        HttpServletRequest req = (HttpServletRequest) configurations.get("request");
        
        genreRepository = new GenreRepository();
        
        if(parameters.get("nom") != null) {
            genreRepository.addGenre((Connection)servletContext.getAttribute("conn"), parameters.get("nom").trim());
        }
        
        req.setAttribute("listeGenre", genreRepository.getGenreList((Connection)servletContext.getAttribute("conn")));
        
        return new ActionFlow(vue, vue+".jsp", false);
    }

}
