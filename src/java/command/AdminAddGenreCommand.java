package command;

import java.sql.Connection;
import java.util.HashMap;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import model.Document;
import model.DocumentRepository;
import model.GenreRepository;
import model.TypeRepository;

public class AdminAddGenreCommand extends Command {

    private GenreRepository genreRepository;

    @Override
    public String getCommandName() {
        return "admin-add-genre";
    }
    
    @Override
    public ActionFlow execute(HashMap<String, String> parameters, HashMap<String, Object> configurations) {
        String vue = "admin-add-genre";
        
        return new ActionFlow(vue, vue+".jsp", false);
    }

}
