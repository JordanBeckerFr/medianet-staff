package command;

import java.sql.Connection;
import java.util.HashMap;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import model.DocumentRepository;

public class AdminDocumentCommand extends Command {

    private DocumentRepository documentRepository;

    @Override
    public String getCommandName() {
        return "admin-document";
    }
    
    @Override
    public ActionFlow execute(HashMap<String, String> parameters, HashMap<String, Object> configurations) {
        String vue = "admin-document";
        
        ServletContext servletContext = (ServletContext) configurations.get("servletContext");
        HttpServletRequest req = (HttpServletRequest) configurations.get("request");
        
        documentRepository = new DocumentRepository();
        
        if(parameters.get("titre") != null) {
            documentRepository.addDocument((Connection)servletContext.getAttribute("conn"), parameters);
        }
        
        req.setAttribute("listeDocument", documentRepository.getDocumentList((Connection)servletContext.getAttribute("conn")));
        
        return new ActionFlow(vue, vue+".jsp", false);
    }

}
